package com.example.desafiobrubank.Main.Presenter;

import android.content.Context;

import com.example.desafiobrubank.Main.Model.OverViewContract;
import com.example.desafiobrubank.Main.Model.OverviewActivityModel;
import com.example.desafiobrubank.Main.View.OverviewActivity;
import com.example.desafiobrubank.Utils.Model.Gender;
import com.example.desafiobrubank.Utils.Model.TVShow;

import java.util.ArrayList;
import java.util.List;

public class OverviewActivityPresenter implements OverViewContract.Presenter {

    private OverviewActivity view;
    private OverviewActivityModel model;

    public OverviewActivityPresenter(OverviewActivity activity) {
        this.view = activity;
        this.model = new OverviewActivityModel(this);
    }

    @Override
    public void getListTvShows() {
        model.getRateTVShows();
    }

    @Override
    public void obtenerListTvShows(List<TVShow> listTvShow) {
        view.chargeListTvShows(listTvShow);
    }

    @Override
    public void getListGenderTVShows() {
        model.getListGenderTVShows();
    }

    @Override
    public void obtenerListGenderTVShows(List<Gender> listGender) {
        view.chargeListGender(listGender);
    }

    @Override
    public Context getContext() {
        return view.getApplicationContext();
    }

    @Override
    public List<TVShow> getListTVShowFavorites() {
        List<TVShow> list = model.getListTVShowFavorites();
        if (list.size() == 0){
            view.showSinSuscripciones();
            return new ArrayList<>();
        }else {
            view.showRecyclerViewSuscriptas();
            return list;
        }
    }

    @Override
    public void insertTVShow(TVShow tvShow) {
        model.insertTVShow(tvShow);
    }

    @Override
    public void deleteTVShow(TVShow tvShow) {
        model.deleteTVShow(tvShow);
    }

    @Override
    public boolean verifyIfSuscription(TVShow tvShow) {
        TVShow tvShow1 = model.getTVShowForId(tvShow.getId());
        return tvShow1 != null;
    }
}

package com.example.desafiobrubank.Main.Model;

import android.content.Context;

import com.example.desafiobrubank.Utils.Model.Gender;
import com.example.desafiobrubank.Utils.Model.TVShow;

import java.util.List;

public interface OverViewContract {

    interface View{
        void chargeListTvShows(List<TVShow> listTVShow);
        void chargeListGender(List<Gender> listGender);
        void showSinSuscripciones();
        void showRecyclerViewSuscriptas();
    }

    interface Presenter{
        void getListTvShows();
        void obtenerListTvShows(List<TVShow> listTvShow);
        void getListGenderTVShows();
        void obtenerListGenderTVShows(List<Gender> listGender);
        Context getContext();
        List<TVShow> getListTVShowFavorites();
        void insertTVShow(TVShow tvShow);
        void deleteTVShow(TVShow tvShow);
        boolean verifyIfSuscription(TVShow tvShow);
    }

    interface Model{
        void getRateTVShows();
        void getListGenderTVShows();
        List<TVShow> getListTVShowFavorites();
        void insertTVShow(TVShow tvShow);
        void deleteTVShow(TVShow tvShow);
        TVShow getTVShowForId(int id);
    }
}

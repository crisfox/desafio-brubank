package com.example.desafiobrubank.Main.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.desafiobrubank.Details.View.DetailActivity;
import com.example.desafiobrubank.Utils.Adapters.AdapterList;
import com.example.desafiobrubank.Search.View.SearchFragment;
import com.example.desafiobrubank.Main.Model.OverViewContract;
import com.example.desafiobrubank.Main.Presenter.OverviewActivityPresenter;
import com.example.desafiobrubank.R;
import com.example.desafiobrubank.Utils.Model.Gender;
import com.example.desafiobrubank.Utils.Model.TVShow;
import com.example.desafiobrubank.Utils.SessionManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.desafiobrubank.Utils.Constants.REQUEST_CODE_ACTIVITY;

public class OverviewActivity extends AppCompatActivity implements OverViewContract.View, AdapterList.ListenerAdapter, SearchFragment.ListenerClickTVShow{


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageButtonSearch)
    ImageButton imageButtonSearch;
    @BindView(R.id.containerSearch)
    FrameLayout containerSearch;
    @BindView(R.id.recyclerViewOverview)
    RecyclerView recyclerViewOverView;
    @BindView(R.id.recyclerViewSuscriptas)
    RecyclerView recyclerViewSuscriptas;
    @BindView(R.id.linearSuscriptas)
    LinearLayout linearSuscriptas;

    private SessionManager sessionManager;
    private OverviewActivityPresenter presenter;
    private AdapterList adapterListOverview;
    private AdapterList adapterListSuscriptas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        init();
        setToolbar();
        setButtonSearch();
        setupRecyclerOverview();
        setupRecyclerSuscriptas();
        getLists();
    }

    private void init() {
        presenter = new OverviewActivityPresenter(this);
        sessionManager = SessionManager.getInstance();
    }

    private void getLists() {
        presenter.getListTvShows();
        presenter.getListGenderTVShows();
    }

    private void setupRecyclerOverview() {
        adapterListOverview = new AdapterList(this,R.layout.cell_overview);
        recyclerViewOverView.setHasFixedSize(true);
        recyclerViewOverView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerViewOverView.setAdapter(adapterListOverview);
    }

    private void setupRecyclerSuscriptas() {
        adapterListSuscriptas = new AdapterList(this,R.layout.cell_suscriptas);
        recyclerViewSuscriptas.setHasFixedSize(true);
        recyclerViewSuscriptas.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewSuscriptas.setAdapter(adapterListSuscriptas);
        adapterListSuscriptas.loadListTvShow(presenter.getListTVShowFavorites());
    }

    private void setButtonSearch() {
        imageButtonSearch.setOnClickListener(v -> clickSearch());
    }

    private void setToolbar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    private void clickSearch() {
        containerSearch.setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.containerSearch, SearchFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void chargeListTvShows(List<TVShow> listTVShow) {
        adapterListOverview.loadListTvShow(listTVShow);
    }

    @Override
    public void chargeListGender(List<Gender> listGender) {
        sessionManager.setListGender(listGender);
    }

    @Override
    public void showSinSuscripciones() {
        linearSuscriptas.setVisibility(View.GONE);
    }

    @Override
    public void showRecyclerViewSuscriptas() {
        linearSuscriptas.setVisibility(View.VISIBLE);
    }


    @Override
    public void clickTVShow(TVShow tvShow) {
        DetailActivity.navigateWithResult(this,tvShow,REQUEST_CODE_ACTIVITY,presenter.verifyIfSuscription(tvShow));
    }

    @Override
    public void addSuscripto(TVShow tvShow) {
        presenter.insertTVShow(tvShow);
    }

    @Override
    public void removeSuscripto(TVShow tvShow) {
        presenter.deleteTVShow(tvShow);
    }

    @Override
    public boolean verifyIfSuscription(TVShow tvShow) {
        return presenter.verifyIfSuscription(tvShow);
    }

    @Override
    public void refreshListBack() {
        adapterListSuscriptas.loadListTvShow(presenter.getListTVShowFavorites());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == REQUEST_CODE_ACTIVITY)
                refreshListBack();
        }
    }

}

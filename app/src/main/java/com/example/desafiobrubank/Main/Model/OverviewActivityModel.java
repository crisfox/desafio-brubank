package com.example.desafiobrubank.Main.Model;

import androidx.annotation.Nullable;

import com.example.desafiobrubank.Main.Presenter.OverviewActivityPresenter;
import com.example.desafiobrubank.Services.AppDatabaseRoom;
import com.example.desafiobrubank.Utils.Model.ContainerGender;
import com.example.desafiobrubank.Utils.Model.ContainerTVShow;
import com.example.desafiobrubank.Utils.Model.Gender;
import com.example.desafiobrubank.Utils.Model.TVShow;
import com.example.desafiobrubank.Services.RetrofitService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OverviewActivityModel implements OverViewContract.Model{

    private RetrofitService retrofitService;
    private OverviewActivityPresenter presenter;
    private AppDatabaseRoom db;

    public OverviewActivityModel(OverviewActivityPresenter presenter) {
        this.presenter = presenter;
        this.retrofitService = new RetrofitService();
        db = AppDatabaseRoom.getInstance(presenter.getContext());
    }

    @Override
    public void getRateTVShows() {
        final Call<ContainerTVShow> batch = retrofitService.getListRateTVShows(1);
        batch.enqueue(new Callback<ContainerTVShow>() {
            @Override
            public void onResponse(@Nullable Call<ContainerTVShow> call, @Nullable Response<ContainerTVShow> response) {
                if (response != null && response.body() != null)
                {
                    List<TVShow> listTvShow = response.body().getListTVShow();
                    presenter.obtenerListTvShows(listTvShow);
                }
            }

            @Override
            public void onFailure(@Nullable Call<ContainerTVShow> call, @Nullable Throwable t) {
                if(t!=null)
                {
                    t.printStackTrace();
                }
            }
        });
    }

    @Override
    public void getListGenderTVShows() {
        final Call<ContainerGender> batch = retrofitService.getGenderTVShows();
        batch.enqueue(new Callback<ContainerGender>() {
            @Override
            public void onResponse(@Nullable Call<ContainerGender> call, @Nullable Response<ContainerGender> response) {
                if (response != null && response.body() != null)
                {
                    List<Gender> listGender = response.body().getListGender();
                    presenter.obtenerListGenderTVShows(listGender);
                }
            }

            @Override
            public void onFailure(@Nullable Call<ContainerGender> call, @Nullable Throwable t) {
                if(t!=null)
                {
                    t.printStackTrace();
                }
            }
        });
    }

    @Override
    public List<TVShow> getListTVShowFavorites() {
        return db.tvShowDao().getAllFavorites();
    }

    @Override
    public void insertTVShow(TVShow tvShow) {
        db.tvShowDao().insertAll(tvShow);
    }

    @Override
    public void deleteTVShow(TVShow tvShow) {
        db.tvShowDao().delete(tvShow);
    }

    @Override
    public TVShow getTVShowForId(int id) {
        return db.tvShowDao().getTVForId(id);
    }
}

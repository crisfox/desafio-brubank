package com.example.desafiobrubank.Details.Model;

import com.example.desafiobrubank.Details.Presenter.DetailPresenter;
import com.example.desafiobrubank.Services.AppDatabaseRoom;
import com.example.desafiobrubank.Utils.Model.TVShow;

public class DetailModel implements DetailContract.Model {

    private AppDatabaseRoom db;

    public DetailModel(DetailPresenter presenter) {
        db = AppDatabaseRoom.getInstance(presenter.getContext());
    }

    @Override
    public void insertTVShow(TVShow tvShow) {
        db.tvShowDao().insertAll(tvShow);
    }

    @Override
    public void deleteTVShow(TVShow tvShow) {
        db.tvShowDao().delete(tvShow);
    }
}

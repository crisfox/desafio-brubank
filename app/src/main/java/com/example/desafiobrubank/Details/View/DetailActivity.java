package com.example.desafiobrubank.Details.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.palette.graphics.Palette;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.desafiobrubank.Details.Model.DetailContract;
import com.example.desafiobrubank.Details.Presenter.DetailPresenter;
import com.example.desafiobrubank.Main.View.OverviewActivity;
import com.example.desafiobrubank.R;
import com.example.desafiobrubank.Utils.Model.TVShow;
import com.google.android.material.appbar.AppBarLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.desafiobrubank.Utils.Constants.SUSCRIPTO;
import static com.example.desafiobrubank.Utils.Constants.TV_SHOW;
import static com.example.desafiobrubank.Utils.Constants.URL_IMAGE;
import static com.example.desafiobrubank.Utils.Utils.adjustAlpha;
import static com.example.desafiobrubank.Utils.Utils.obtenerYear;


public class DetailActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, DetailContract.View {

    @BindView(R.id.main_toolbar)
    Toolbar toolbar;
    @BindView(R.id.linearLayoutToolbar)
    LinearLayout linearLayoutToolbar;
    @BindView(R.id.cardViewDetailChico)
    CardView cardViewImageChica;
    @BindView(R.id.main_appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.main_linearlayout_title)
    LinearLayout mTitleContainer;
    @BindView(R.id.buttonBack)
    ImageButton imageButtonBack;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.textViewYear)
    TextView textViewYear;
    @BindView(R.id.imageViewBackground)
    ImageView imageViewBackground;
    @BindView(R.id.frameLayoutBackground)
    FrameLayout frameLayoutBackground;
    @BindView(R.id.imageViewDetail)
    ImageView imageViewDetail;
    @BindView(R.id.imageViewDetailChico)
    ImageView imageViewDetailChico;
    @BindView(R.id.textViewDescriptionResume)
    TextView textViewDescriptionResume;
    @BindView(R.id.textViewButtonSuscripto1)
    TextView textViewButtonSuscripto1;
    @BindView(R.id.textViewButtonAgregadoQuitar1)
    TextView textViewButtonAgregadoQuitar1;
    @BindView(R.id.textViewButtonSuscripto2)
    TextView textViewButtonSuscripto2;
    @BindView(R.id.textViewButtonAgregadoQuitar2)
    TextView textViewButtonAgregadoQuitar2;

    private TVShow tvShow;
    private boolean suscripto;
    private DetailPresenter presenter;

    public static void navigateWithResult(OverviewActivity activity, TVShow tvShow, int requestCode, boolean suscripto) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(TV_SHOW, tvShow);
        intent.putExtra(SUSCRIPTO, suscripto);
        ActivityCompat.startActivityForResult(activity, intent, requestCode, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        getDataIntent();
        setBack();
        setPresenter(this);
        setupListener(textViewButtonAgregadoQuitar1);
        setupListener(textViewButtonAgregadoQuitar2);
        setupListener(textViewButtonSuscripto1);
        setupListener(textViewButtonSuscripto2);
        appBarLayout.addOnOffsetChangedListener(this);
        startAlphaAnimation(toolbar, 0, View.INVISIBLE);
    }

    private void setPresenter(DetailActivity detailActivity) {
        presenter = new DetailPresenter(detailActivity);
    }

    private void getDataIntent() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            tvShow = bundle.getParcelable(TV_SHOW);
            suscripto = bundle.getBoolean(SUSCRIPTO);
            setView();
        }
    }

    public void getBitmap(String image) {
        Picasso.get()
                .load(URL_IMAGE + image)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        createPaletteAsync(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    }
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
    }

    private void setBack() {
        imageButtonBack.setOnClickListener(v -> onBackPressed());
    }

    private void setView() {
        if (suscripto) {
            changeButtonSuscription(View.GONE, View.VISIBLE);
        } else {
            changeButtonSuscription(View.VISIBLE, View.GONE);
        }
        Picasso.get().load(URL_IMAGE + tvShow.getPoster_path()).fit().into(imageViewDetail);
        textViewTitle.setText(tvShow.getOriginal_name());
        if (tvShow.getFirst_air_date() != null)
            textViewYear.setText(obtenerYear(tvShow.getFirst_air_date()));
        textViewDescriptionResume.setText(tvShow.getOverview());
        Picasso.get().load(URL_IMAGE + tvShow.getPoster_path()).fit().into(imageViewDetail);
        Picasso.get().load(URL_IMAGE + tvShow.getPoster_path()).fit().into(imageViewDetailChico);
        Picasso.get().load(URL_IMAGE + tvShow.getPoster_path()).fit().into(imageViewBackground);
        getBitmap(tvShow.getPoster_path());
        int halfTransparentColor = adjustAlpha(getColor(R.color.colorBlackBackground), 0.6f);
        toolbar.setBackgroundColor(getColor(R.color.colorBlackBackground));
        frameLayoutBackground.setBackgroundColor(halfTransparentColor);

    }

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.6f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;

    private boolean mIsTheTitleVisible = false;


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

        handleToolbarTitleVisibility(percentage);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    public void createPaletteAsync(Bitmap bitmap) {
        Palette.from(bitmap).generate(p -> {
            if (p.getDarkVibrantSwatch() != null) {
                Palette.Swatch rgb = p.getDarkVibrantSwatch();
                int halfTransparentColor = getColor(R.color.colorGrayTransparentOverView);
                int halfColor = halfTransparentColor;
                if (rgb != null) {
                    halfTransparentColor = adjustAlpha(rgb.getRgb(), 0.9f);
                    halfColor = rgb.getRgb();
                }
                toolbar.setBackgroundColor(halfColor);
                startAlphaAnimation(toolbar, 0, View.INVISIBLE);
                frameLayoutBackground.setBackgroundColor(halfTransparentColor);
                textViewButtonAgregadoQuitar1.setTextColor(halfTransparentColor);
            }
        });
    }

    private void changeButtonSuscription(int suscription1, int suscription2) {
        textViewButtonSuscripto1.setVisibility(suscription1);
        textViewButtonAgregadoQuitar1.setVisibility(suscription2);
        textViewButtonSuscripto2.setVisibility(suscription1);
        textViewButtonAgregadoQuitar2.setVisibility(suscription2);
    }

    private void setupListener(TextView view) {
        view.setOnClickListener(v -> {
            if (suscripto) {
                presenter.deleteTVShow(tvShow);
            } else {
                presenter.insertTVShow(tvShow);
            }
        });
    }

    @Override
    public void changeSuscription() {
        if (suscripto) {
            changeButtonSuscription(View.VISIBLE, View.GONE);
            suscripto = false;
        } else {
            changeButtonSuscription(View.GONE, View.VISIBLE);
            suscripto = true;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }
}

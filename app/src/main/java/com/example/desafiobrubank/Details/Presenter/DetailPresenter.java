package com.example.desafiobrubank.Details.Presenter;

import android.content.Context;

import com.example.desafiobrubank.Details.Model.DetailContract;
import com.example.desafiobrubank.Details.Model.DetailModel;
import com.example.desafiobrubank.Details.View.DetailActivity;
import com.example.desafiobrubank.Utils.Model.TVShow;

public class DetailPresenter implements DetailContract.Presenter {

    private DetailActivity view;
    private DetailModel model;

    public DetailPresenter(DetailActivity activity) {
        this.view = activity;
        this.model = new DetailModel(this);
    }

    @Override
    public void insertTVShow(TVShow tvShow) {
        model.insertTVShow(tvShow);
        view.changeSuscription();
    }

    @Override
    public void deleteTVShow(TVShow tvShow) {
        model.deleteTVShow(tvShow);
        view.changeSuscription();
    }

    @Override
    public Context getContext() {
        return view.getApplicationContext();
    }
}

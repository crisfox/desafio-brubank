package com.example.desafiobrubank.Details.Model;

import android.content.Context;

import com.example.desafiobrubank.Utils.Model.TVShow;

public interface DetailContract {
    interface View{
        void changeSuscription();
    }

    interface Presenter{
        void insertTVShow(TVShow tvShow);
        void deleteTVShow(TVShow tvShow);
        Context getContext();
    }

    interface Model{
        void insertTVShow(TVShow tvShow);
        void deleteTVShow(TVShow tvShow);
    }
}

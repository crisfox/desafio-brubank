package com.example.desafiobrubank.Search.View;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.desafiobrubank.Utils.Adapters.AdapterList;
import com.example.desafiobrubank.R;
import com.example.desafiobrubank.Search.Model.SearchContract;
import com.example.desafiobrubank.Search.Presenter.SearchPresenter;
import com.example.desafiobrubank.Utils.Model.TVShow;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFragment extends Fragment implements SearchContract.View, AdapterList.ListenerAdapter {

    @BindView(R.id.textButtonBack)
    TextView textButtonBack;
    @BindView(R.id.editTextSearch)
    EditText editTextSearch;
    @BindView(R.id.recyclerViewSearch)
    RecyclerView recyclerViewSearch;
    @BindView(R.id.textViewSearch)
    TextView textViewSearch;

    private AdapterList adapter;
    private SearchPresenter presenter;
    private ListenerClickTVShow listener;

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (ListenerClickTVShow) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment, container, false);
        ButterKnife.bind(this,view);
        presenter = new SearchPresenter(this);
        setViews();

        setupSearch();
        setupRecyclerSearch();

        return view;
    }

    private void setupRecyclerSearch() {
        adapter = new AdapterList(this,R.layout.cell_search);
        recyclerViewSearch.setHasFixedSize(true);
        recyclerViewSearch.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerViewSearch.setAdapter(adapter);
    }


    private void setupSearch() {
        editTextSearch.requestFocus();
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.searchTvShow(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void setViews() {
        textButtonBack.setOnClickListener(v -> {
            if (getFragmentManager() !=null){
                getFragmentManager().popBackStack();
                listener.refreshListBack();
            }
        });
    }

    @Override
    public void showResultSearch(List<TVShow> listTvShow) {
        adapter.loadListTvShow(listTvShow);
        recyclerViewSearch.setVisibility(View.VISIBLE);
        textViewSearch.setVisibility(View.GONE);
    }

    @Override
    public void showSearchResultEmpty() {
        textViewSearch.setVisibility(View.VISIBLE);
        recyclerViewSearch.setVisibility(View.GONE);
    }

    @Override
    public void clickTVShow(TVShow tvShow) {
        listener.clickTVShow(tvShow);
    }

    @Override
    public void addSuscripto(TVShow tvShow) {
        listener.addSuscripto(tvShow);
    }

    @Override
    public void removeSuscripto(TVShow tvShow) {
        listener.removeSuscripto(tvShow);
    }

    @Override
    public boolean verifyIfSuscription(TVShow tvShow) {
        return listener.verifyIfSuscription(tvShow);
    }

    public interface ListenerClickTVShow{
        void clickTVShow(TVShow tvShow);
        void addSuscripto(TVShow tvShow);
        void removeSuscripto(TVShow tvShow);
        void refreshListBack();
        boolean verifyIfSuscription(TVShow tvShow);
    }
}

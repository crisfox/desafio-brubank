package com.example.desafiobrubank.Search.Model;

import androidx.annotation.Nullable;

import com.example.desafiobrubank.Search.Presenter.SearchPresenter;
import com.example.desafiobrubank.Services.RetrofitService;
import com.example.desafiobrubank.Utils.Model.ContainerTVShow;
import com.example.desafiobrubank.Utils.Model.TVShow;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchModel implements SearchContract.Model {

    private SearchPresenter presenter;
    private RetrofitService retrofitService;

    public SearchModel(SearchPresenter searchPresenter) {
        this.presenter = searchPresenter;
        this.retrofitService = new RetrofitService();
    }

    @Override
    public void getSearchTvShow(String searchText) {
        final Call<ContainerTVShow> batch = retrofitService.searchTVShows(searchText,1);
        batch.enqueue(new Callback<ContainerTVShow>() {
            @Override
            public void onResponse(@Nullable Call<ContainerTVShow> call, @Nullable Response<ContainerTVShow> response) {
                if (response != null && response.body() != null)
                {
                    List<TVShow> listTvShow = response.body().getListTVShow();
                    presenter.getSearchTvShow(listTvShow);
                }
            }

            @Override
            public void onFailure(@Nullable Call<ContainerTVShow> call, @Nullable Throwable t) {
                if(t!=null)
                {
                    t.printStackTrace();
                }
            }
        });
    }


}

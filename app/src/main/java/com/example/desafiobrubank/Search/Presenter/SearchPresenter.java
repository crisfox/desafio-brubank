package com.example.desafiobrubank.Search.Presenter;

import android.content.Context;

import com.example.desafiobrubank.Search.Model.SearchContract;
import com.example.desafiobrubank.Search.Model.SearchModel;
import com.example.desafiobrubank.Search.View.SearchFragment;
import com.example.desafiobrubank.Utils.Model.TVShow;

import java.util.List;

public class SearchPresenter implements SearchContract.Presenter {

    private SearchFragment fragment;
    private SearchModel model;

    public SearchPresenter(SearchFragment searchFragment) {
        this.fragment = searchFragment;
        model = new SearchModel(this);
    }

    @Override
    public void searchTvShow(String searchText) {
        model.getSearchTvShow(searchText);
    }

    @Override
    public void getSearchTvShow(List<TVShow> listTVShow) {
        if (!listTVShow.isEmpty()){
            fragment.showResultSearch(listTVShow);
        }else {
            fragment.showSearchResultEmpty();
        }
    }

    @Override
    public Context getContext() {
        return fragment.getContext();
    }
}

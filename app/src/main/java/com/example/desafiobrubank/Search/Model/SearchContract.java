package com.example.desafiobrubank.Search.Model;

import android.content.Context;

import com.example.desafiobrubank.Utils.Model.TVShow;

import java.util.List;

public interface SearchContract {
    interface View{
        void showResultSearch(List<TVShow> listTvShow);
        void showSearchResultEmpty();
    }

    interface Presenter{
        void searchTvShow(String searchText);
        void getSearchTvShow(List<TVShow> listTVShow);
        Context getContext();
    }

    interface Model{
        void getSearchTvShow(String searchText);
    }
}

package com.example.desafiobrubank.Services;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.desafiobrubank.Utils.Model.TVShow;

import java.util.List;

@Dao
public interface TVShowDao {
    @Query("SELECT * FROM tv_show")
    List<TVShow> getAllFavorites();

    @Query("SELECT * FROM tv_show WHERE id = :id")
    TVShow getTVForId(int id);

    @Insert
    void insertAll(TVShow... users);

    @Delete
    void delete(TVShow user);
}

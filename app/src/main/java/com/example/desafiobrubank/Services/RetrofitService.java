package com.example.desafiobrubank.Services;

import com.example.desafiobrubank.Utils.Model.ContainerGender;
import com.example.desafiobrubank.Utils.Model.ContainerTVShow;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.desafiobrubank.Utils.Constants.API_KEY;
import static com.example.desafiobrubank.Utils.Constants.BASE_URL;
import static com.example.desafiobrubank.Utils.Constants.LANGUAGE;

public class RetrofitService {
    private Retrofit retrofit;

    public RetrofitService() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Call<ContainerTVShow> getListRateTVShows(Integer page) {
        Service service = retrofit.create(Service.class);
        return service.getListRateTVShows(API_KEY, LANGUAGE, page);
    }

    public Call<ContainerGender> getGenderTVShows() {
        Service service = retrofit.create(Service.class);
        return service.getGenderTVShows(API_KEY);
    }

    public Call<ContainerTVShow> searchTVShows(String searchText, Integer page) {
        Service service = retrofit.create(Service.class);
        return service.searchTvShow(API_KEY, LANGUAGE, searchText, page);
    }
}

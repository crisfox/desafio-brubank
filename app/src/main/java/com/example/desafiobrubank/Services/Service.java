package com.example.desafiobrubank.Services;

import com.example.desafiobrubank.Utils.Model.ContainerGender;
import com.example.desafiobrubank.Utils.Model.ContainerTVShow;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface Service {

    @Headers({"Accept: application/json",
            "Content-Type: application/json"})
    @GET("genre/tv/list")
    Call<ContainerGender> getGenderTVShows(@Query("api_key") String apiKey);

    @Headers({"Accept: application/json",
            "Content-Type: application/json"})
    @GET("tv/top_rated")
    Call<ContainerTVShow> getListRateTVShows(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") Integer page);

    @Headers({"Accept: application/json",
            "Content-Type: application/json"})
    @GET("search/tv")
    Call<ContainerTVShow> searchTvShow(@Query("api_key") String apiKey, @Query("language") String language, @Query("query") String searchString, @Query("page") Integer page);
}

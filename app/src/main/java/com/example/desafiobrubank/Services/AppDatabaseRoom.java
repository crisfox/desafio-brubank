package com.example.desafiobrubank.Services;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.desafiobrubank.Utils.Model.TVShow;

@Database(entities = {TVShow.class}, version = 1)
public abstract class AppDatabaseRoom extends RoomDatabase {
    private static final String DB_NAME = "tv_show";
    private static AppDatabaseRoom instance;

    public static AppDatabaseRoom getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabaseRoom.class, DB_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract TVShowDao tvShowDao();
}


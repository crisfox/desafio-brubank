package com.example.desafiobrubank.Utils.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContainerGender {
    @SerializedName("genres")
    private List<Gender> listGender;

    public ContainerGender(List<Gender> listGender) {
        this.listGender = listGender;
    }

    public List<Gender> getListGender() {
        return listGender;
    }

}

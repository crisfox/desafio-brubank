package com.example.desafiobrubank.Utils;

public abstract class Constants {

    //Config Services
    public final static String BASE_URL = "https://api.themoviedb.org/3/";
    public final static String LANGUAGE = "es-ES";
    public final static String API_KEY = "3b8e7c41ac3060fb9b20eb3a64d165e5";
    public final static String URL_IMAGE = "https://image.tmdb.org/t/p/w500/";

    //Constantes
    public final static String TV_SHOW = "TVSHOW";
    public static final String SUSCRIPTO = "suscripto";
    public static final Integer REQUEST_CODE_ACTIVITY = 12;


}

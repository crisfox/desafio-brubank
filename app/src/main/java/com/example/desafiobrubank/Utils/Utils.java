package com.example.desafiobrubank.Utils;

import android.graphics.Color;

import androidx.annotation.ColorInt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public abstract class Utils {

    public static String obtenerYear(String date) {
        Date yyyy;
        try {
            yyyy = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(date);
            Calendar calendar = Calendar.getInstance();
            assert yyyy != null;
            calendar.setTime(yyyy);
            return Integer.toString(calendar.get(Calendar.YEAR));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    @ColorInt
    public static int adjustAlpha(@ColorInt int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }
}

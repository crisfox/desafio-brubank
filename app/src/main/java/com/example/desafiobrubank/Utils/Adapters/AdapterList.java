package com.example.desafiobrubank.Utils.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.example.desafiobrubank.R;
import com.example.desafiobrubank.Utils.Model.TVShow;
import com.example.desafiobrubank.Utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.desafiobrubank.Utils.Constants.URL_IMAGE;

public class AdapterList extends Adapter {

    private List<TVShow> listRateTVShow = new ArrayList<>();
    private ListenerAdapter listenerAdapter;
    private int layout;

    public AdapterList(ListenerAdapter listenerAdapter, int layout) {
        this.listenerAdapter = listenerAdapter;
        this.layout = layout;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View cell = layoutInflater.inflate(layout, parent, false);
        return new AdapterOverviewViewHolder(cell);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TVShow tvShow = listRateTVShow.get(position);
        AdapterOverviewViewHolder viewHolder = (AdapterOverviewViewHolder) holder;
        if (layout == R.layout.cell_suscriptas) {
            viewHolder.setTvShowSuscriptas(tvShow);
        } else if (layout == R.layout.cell_overview) {
            viewHolder.setTvShowOverview(tvShow);
        } else if (layout == R.layout.cell_search) {
            viewHolder.setTvShowSearch(tvShow);
        }
    }

    @Override
    public int getItemCount() {
        return listRateTVShow.size();
    }

    class AdapterOverviewViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @Nullable
        @BindView(R.id.textViewGender)
        TextView textViewGender;
        @NonNull
        @BindView(R.id.imageViewPoster)
        ImageView imageViewBackgroundOverview;
        @Nullable
        @BindView(R.id.textViewButtonAgregar)
        TextView textViewButtonAgregar;
        @Nullable
        @BindView(R.id.textViewButtonAgregadoQuitar)
        TextView textViewButtonAgregadoQuitar;
        private SessionManager sessionManager = SessionManager.getInstance();

        AdapterOverviewViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setTvShowSearch(TVShow tvShow) {
            if (!tvShow.getOriginal_name().isEmpty()) {
                textViewTitle.setText(tvShow.getOriginal_name());
            }
            if (tvShow.getGenre_ids().size() != 0) {
                textViewGender.setText(sessionManager.obtenerGender(tvShow.getGenre_ids().get(0)));
            }

            if (tvShow.getPoster_path() != null) {
                Picasso.get().load(URL_IMAGE + tvShow.getPoster_path()).placeholder(R.mipmap.ic_netflix).fit().into(imageViewBackgroundOverview);
            }
            if (textViewButtonAgregar != null) {
                textViewButtonAgregar.setOnClickListener(v -> {
                    showButton(View.GONE, View.VISIBLE);
                    listenerAdapter.addSuscripto(tvShow);
                });
            }

            if (textViewButtonAgregadoQuitar != null) {
                textViewButtonAgregadoQuitar.setOnClickListener(v -> {
                    showButton(View.VISIBLE, View.GONE);
                    listenerAdapter.removeSuscripto(tvShow);
                });
            }
            itemView.setOnClickListener(v -> listenerAdapter.clickTVShow(tvShow));

            if (listenerAdapter.verifyIfSuscription(tvShow)) {
                showButton(View.GONE, View.VISIBLE);
            } else {
                showButton(View.VISIBLE, View.GONE);
            }

        }

        private void showButton(int agregar, int quitar) {
            textViewButtonAgregar.setVisibility(agregar);
            textViewButtonAgregadoQuitar.setVisibility(quitar);
        }

        void setTvShowOverview(TVShow tvShow) {
            if (!tvShow.getOriginal_name().isEmpty()) {
                textViewTitle.setText(tvShow.getOriginal_name());
            }
            if (tvShow.getGenre_ids().size() != 0) {
                textViewGender.setText(sessionManager.obtenerGender(tvShow.getGenre_ids().get(0)));
            }

            if (tvShow.getBackdrop_path() != null) {
                Picasso.get().load(URL_IMAGE + tvShow.getBackdrop_path()).placeholder(R.mipmap.ic_netflix).fit().into(imageViewBackgroundOverview);
            }
            itemView.setOnClickListener(v -> listenerAdapter.clickTVShow(tvShow));
        }

        void setTvShowSuscriptas(TVShow tvShow) {
            if (tvShow.getPoster_path() != null) {
                Picasso.get().load(URL_IMAGE + tvShow.getPoster_path()).placeholder(R.mipmap.ic_netflix).fit().into(imageViewBackgroundOverview);
            }
            itemView.setOnClickListener(v -> listenerAdapter.clickTVShow(tvShow));
        }

    }

    public void loadListTvShow(List<TVShow> list) {
        if (list != null) {
            listRateTVShow.clear();
            listRateTVShow.addAll(list);
            notifyDataSetChanged();
        }
    }

    public interface ListenerAdapter {
        void clickTVShow(TVShow tvShow);
        void addSuscripto(TVShow tvShow);
        void removeSuscripto(TVShow tvShow);
        boolean verifyIfSuscription(TVShow tvShow);
    }

}

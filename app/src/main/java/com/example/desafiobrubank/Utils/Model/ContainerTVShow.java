package com.example.desafiobrubank.Utils.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContainerTVShow {

    @SerializedName("results")
    private List<TVShow> listTVShow;

    public ContainerTVShow(List<TVShow> listTVShow) {
        this.listTVShow = listTVShow;
    }

    public List<TVShow> getListTVShow() {
        return listTVShow;
    }
}

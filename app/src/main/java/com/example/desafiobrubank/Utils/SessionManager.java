package com.example.desafiobrubank.Utils;

import com.example.desafiobrubank.Utils.Model.Gender;

import java.util.ArrayList;
import java.util.List;

public class SessionManager {
    private static final SessionManager INSTANCE = new SessionManager();

    public static SessionManager getInstance() {
        return INSTANCE;
    }

    private List<Gender> listGender = new ArrayList<>();

    public void setListGender(List<Gender> listGender) {
        this.listGender = listGender;
    }

    public String obtenerGender(Integer id) {
        for (Gender gender :
                listGender) {
            if (gender.getId().equals(id)) {
                return gender.getName();
            }
        }
        return "";
    }

}
